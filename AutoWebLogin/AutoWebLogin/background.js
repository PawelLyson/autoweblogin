﻿var masterPassword;

function startBackground() {

    console.log("loadedBackgroundJS");

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.sender === "popupTabCreate") {
            popupTabCreate(request, sender, sendResponse);
        }
        else if (request.sender === "contentTabCreate") {
            contentTabCreate(request, sender, sendResponse);
        }
        else if (request.sender === "popupPassword") {
            rememberPassword(request, sender, sendResponse);
        }
        else if (request.sender === "contentScriptCiphertext") {
            getCiphertextCredentials(request, sender, sendResponse);
        }
        else if (request.sender === "popupGetMasterPassword") {
            sendMasterPassword(request, sender, sendResponse);
        }
        /*else {
            console.log("Brak Adresata");
        }*/
        //sendResponse("ReturnedDataFromBackgroundWorker");
    });
}
function sendMasterPassword(request, sender, sendResponse) {
    sendResponse(masterPassword);
}
function rememberPassword(request, sender, sendResponse) {
    masterPassword = request.pass;
}

function popupTabCreate(request, sender, sendResponse) {
    var tabID = request.targetTab;
    if (typeof startedTabs[tabID] === "undefined") {
        createEmptyStartedTab(tabID);
    }
    startedTabs[tabID]["webInfo"] = request.webInfo;
    startedTabs[tabID]["popupTime"] = request.sendTime;
    isPossibleToLogin(tabID);
}

function contentTabCreate(request, sender, sendResponse) {
    var tabID = sender.tab.id;
    if (typeof startedTabs[tabID] === "undefined") {
        createEmptyStartedTab(tabID);
    }
    //console.log(request.data);
    //console.log("Tab:" + sender.tab.id);
    //console.log(request.sendTime);
    startedTabs[tabID]["contentTime"] = request.sendTime;
    isPossibleToLogin(tabID);
}

var nameCredentials;
var responseTabID;

function getCiphertextCredentials(request, sender, sendResponse) {
    responseTabID = sender.tab.id;
    nameCredentials = "credentials_" + request.passType;
    chrome.storage.local.get(nameCredentials,sendCiphertextCredentials);
}

function sendCiphertextCredentials(storageCiphertext) {
    rp = {};
    var decrypted = JSON.parse(sjcl.decrypt(masterPassword, storageCiphertext[nameCredentials], {}, rp));

    chrome.tabs.sendMessage(responseTabID, { sender: "backgroundCredentials", credentials: decrypted });
}

var startedTabs = [];

function createEmptyStartedTab(tabId) {
    startedTabs[tabId] = {
        "popupTime": 0,
        "contentTime": 0,
        "webInfo": null
    };
}
function isPossibleToLogin(tabID) {

    if (startedTabs[tabID]["popupTime"] !== 0 && startedTabs[tabID]["contentTime"] !== 0 && startedTabs[tabID]["webInfo"] !== null) {
        var diffTime = Math.abs(startedTabs[tabID]["popupTime"] - startedTabs[tabID]["contentTime"]);
        if (diffTime < 60 * 1000) {
            loginToWebsite(tabID);
            startedTabs[tabID]["popupTime"] = 0;
            startedTabs[tabID]["contentTime"] = 0;
        }
    }
    else {
        console.log("Reject");
    }
}

function loginToWebsite(tabID) {
    if (startedTabs[tabID]["webInfo"].passType !== 0) {
        var tabInfo = startedTabs[tabID];
        chrome.tabs.sendMessage(tabID, { sender: "backgroundWorkerActivate", webInfo: tabInfo["webInfo"] });
        //console.log(tabInfo);
        //chrome.tabs.update(tabID, { highlighted: true, selected: true }, loginToWebsite(tabID, tabInfo));
    }
}

startBackground();