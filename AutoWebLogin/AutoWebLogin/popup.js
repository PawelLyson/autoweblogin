﻿var masterPassword;

/*
    Struktura danych JSON:
    {
    "name": "Testowa",
    "url": "onet.pl",
    "passType": 0, //0 brak logowania, "nazwa uwierzytelnienia" logowanie danymi
    "loginElem": "login", //string jquery elementu z textboxem dla wpisania loginu
    "passwordElem": "password", //string jquery elementu z textboxem dla wpisania hasła
    "submitElem": "logged", //string jquery elementu z przyciskiem do wysłania logowania
    "testElem": "userName" //string jquery elementu który może zweryfikował czy poprawnie zalogowano
    }
*/

var webData = ["#nameWeb", "#urlWeb", "#loginType", "#loginElementWeb", "#passwordElementWeb", "#submitElementWeb", "#testWeb"];
var dataCredential = ["#nameCredential", "#loginCredentials", "#passwordCredentials"];
var namesCredentials = [];
var websToAutoLogin = [];


function addEditWebs(event) {

    var changeWeb = event.toElement.id === "changeWeb";
    var removePage = event.toElement.id === "removeWeb";
    var emptyField = false;

    webData.forEach(function (item, index, array) {
        if (isEmptyJQueryString(item)) {
            $(item).parent().addClass('alert-validate');
            emptyField = true;
        }
        if (index === 0) {
            $(item).parent().attr("data-validate", "Podaj nazwę strony");
        }
    });

    if (emptyField) {
        console.log("[ERROR] Empty values");
        return;
    }

    var duplicateName = false;
    var indexDuplicateName = -1;
    websToAutoLogin.forEach(function (value, index, array) {
        if (value.name === getValueFromJQueryString("#nameWeb")) {
            duplicateName = true;
            indexDuplicateName = index;
        }
    });
    if (!changeWeb && !removePage) {
        if (duplicateName) {
            $("#nameWeb").parent().addClass('alert-validate');
            $("#nameWeb").parent().attr("data-validate", "Nazwa się powtarza");
            console.log("[ERROR] Duplicate values");
            return;
        }
        console.log("[INFO] Web add");
    }
    else {
        if (indexDuplicateName > -1) {
            websToAutoLogin.splice(indexDuplicateName, 1);
        }

        if (removePage) 
            console.log("[INFO] Remove web");
        else 
            console.log("[INFO] Change web");
    }

    if (!removePage) {
        websToAutoLogin.push(
            {
                "name": $(webData[0])[0].value,
                "url": $(webData[1])[0].value,
                "passType": $(webData[2])[0].value,
                "loginElem": $(webData[3])[0].value,
                "passwordElem": $(webData[4])[0].value,
                "submitElem": $(webData[5])[0].value,
                "testElem": $(webData[6])[0].value
            });
    }
    chrome.storage.local.set({ "websToAutoLogin": websToAutoLogin });
    cleanWebForm();
}

function loadStorageName() {
    var nameList = $("#loginNameSelect");
    websToAutoLogin.forEach(function (value, index, array) {
            nameList.append($("<option>" + value.name + "</option>"))
    });
}
var nameCredential = null;

function loadCredentialsForm(eventData) {
    var selectedName = eventData.params.data.text;

    nameCredential = "credentials_" + selectedName;
    chrome.storage.local.get(nameCredential, loadCredentialsForForm);
    $(dataCredential[0])[0].value = selectedName;
}

function loadCredentialsForForm(data) {
    var encryptedCredential = data[nameCredential];
    var rp = {};
    var decryptedCredential = JSON.parse(sjcl.decrypt(masterPassword, encryptedCredential, {}, rp));
        $(dataCredential[1])[0].value = decryptedCredential.login;
        $(dataCredential[2])[0].value = decryptedCredential.password;
    
}

function cleanWebForm() {
    $('#credentialsSelect').val("Wybierz tożsamość");
    $('#credentialsSelect').trigger('change');
    webData.forEach(function (value, index, array) {
        cleanJQueryElementInput(value);
    });
}

function cleanCredentialsForm() {
    $('#loginNameSelect').val("Wybierz nazwę strony");
    $('#loginNameSelect').trigger('change');
    $('#loginType').val("Wybierz tożsamość");
    $('#loginType').trigger('change');
    dataCredential.forEach(function (value, index, array) {
        cleanJQueryElementInput(value);
    });
}

function loadWebForm(eventData) {
    var selectedName = eventData.params.data.text;
    websToAutoLogin.forEach(function (value, index, array) {
        if (value.name === selectedName) {
            $(webData[0])[0].value = value.name;
            $(webData[1])[0].value = value.url;
            // select2 jest to nowy element z jquery. jego obsługa różni się od zwykłych elementów input (odczyt można wykonać tak samo)
            $(webData[2]).val(value.passType);
            $(webData[2]).trigger('change');

            $(webData[3])[0].value = value.loginElem;
            $(webData[4])[0].value = value.passwordElem;
            $(webData[5])[0].value = value.submitElem;
            $(webData[6])[0].value = value.testElem;
        }
    });
}

function openWebs() {
    console.log("[INFO]Opening webs");
    var actualDate = new Date();
    websToAutoLogin.forEach(function(value,index,array){
        chrome.tabs.create({ url: value.url, active: false }, function (tab) {
            chrome.runtime.sendMessage({ sender: "popupTabCreate", webInfo: value, targetTab: tab.id, sendTime: actualDate.getTime() }, null);
        });
    });
}

function addEditCredentials(event) {
    var addNewCredentials = event.toElement.id === "addCredentials";
    var removePage = event.toElement.id === "removeCredentials";

    var emptyField = false;
    dataCredential.forEach(function (item, index, array) {
        if (isEmptyJQueryString(item)) {
            $(item).parent().addClass('alert-validate');
            emptyField = true;
        }
    });

    $(dataCredential[0]).parent().attr("data-validate", "Podaj nazwę tożsamości");

    if (emptyField) {
        console.log("[ERROR] Empty values");
        return;
    }

    var nameCredential = "credentials_" + getValueFromJQueryString(dataCredential[0]);

    var indexDuplicateName = namesCredentials.indexOf(getValueFromJQueryString(dataCredential[0]));
    console.log(indexDuplicateName);
    if (addNewCredentials && !removePage) {
        if (indexDuplicateName > -1) {
            $(dataCredential[0]).parent().addClass('alert-validate');
            $(dataCredential[0]).parent().attr("data-validate", "Nazwa się powtarza");
            console.log("[ERROR] Duplicate values");
            return;
        }
        console.log("[INFO]Credentials add");
    }
    else {
        if (indexDuplicateName > -1) {
            console.log(namesCredentials);
            namesCredentials.splice(indexDuplicateName, 1);
            console.log(namesCredentials);
            chrome.storage.local.remove(nameCredential);
        }
        if (removePage)
            console.log("[INFO]Credentials remove");
        else
            console.log("[INFO]Credentials edit");

    }
    if (!removePage) {
        var credential = JSON.stringify({ "login": getValueFromJQueryString(dataCredential[1]), "password": getValueFromJQueryString(dataCredential[2]) });
        encryptAndSaveCredentials(nameCredential, credential);
        namesCredentials.push(getValueFromJQueryString("#nameCredential"));
    }
    chrome.storage.local.set({ "credentials": namesCredentials }, loadNameCredential);
    cleanCredentialsForm();
}

function encryptAndSaveCredentials(nameCredential ,loginCredentials) {
    var p = {
        adata: "weryfikacja",
        iter: 1000,
        mode: "ccm",
        ts: 64,
        ks: 128
    };
    var rp = {};
    var ciphertextCredentials = sjcl.encrypt(masterPassword, loginCredentials, p, rp);
    var toSaveCredentials = {};
    toSaveCredentials[nameCredential] = ciphertextCredentials;
    console.log(toSaveCredentials);
    saveCipherCredentials(toSaveCredentials);
    loadNameCredential();
}

function saveCipherCredentials(toSaveCredentials) {
    chrome.storage.local.set(toSaveCredentials);
}

function loadNameCredential() {
    chrome.storage.local.get("credentials", prepareSaveNameCredential);
}

function prepareSaveNameCredential(items) {
    if (elementIsEmpty(items)) {
        namesCredentials = [];
        return;
    }
    else {
        namesCredentials = items["credentials"];
    }
}

function setMasterPassword(password) {
    masterPassword = password;
    chrome.storage.local.get("testingMaster", loadPasswordTest);
}

function loadWebsToAutoLogin(webs) {
    if (typeof webs["websToAutoLogin"] === "undefined") {
        websToAutoLogin = [];
    }
    else {
        websToAutoLogin = webs["websToAutoLogin"];
    }
}

function loadCredentalsForSelect(credentialWebs) {
    if (typeof credentialWebs["credentials"] !== "undefined") {
        namesCredentials = credentialWebs["credentials"];
        var option = null;
        credentialWebs["credentials"].forEach(function (element) {
            option = "<option>" + element + "</option>";
            $("#loginType").append($(option));
            $("#credentialsSelect").append($(option));
        });
    }
}

function holdMasterPassword() {
    masterPassword = getValueFromJQueryString("#masterPassword");
    chrome.storage.local.get("testingMaster", loadPasswordTest);
}

function loadPasswordTest(testItem) {
    if (typeof testItem["testingMaster"] !== "undefined") {
        var testMessage = testItem["testingMaster"];
        rp = {};
        try {
            var decryptMsg = sjcl.decrypt(masterPassword, testMessage, {}, rp);
            if (decryptMsg === "Sprwadzanie glownego hasla") {
                chrome.runtime.sendMessage({ sender: "popupPassword", pass: masterPassword }, null);
                loginToExtension();
            }
        }
        catch (err) {
            console.log("[ERROR] No Valid MasterPassword");
            $("#masterPassword").parent().addClass('alert-validate');
        }

    }
    else {
        password = $("#masterPassword")[0].value;
        if (elementIsEmpty(password)) {
            console.log("[ERROR]No Master Password");
            $("#masterPassword").parent().addClass('alert-validate');
            return;
        }
        var p = {
            adata: "weryfikacja",
            iter: 1000,
            mode: "ccm",
            ts: 64,
            ks: 128
        };
        var rp = {};
        var ciphertext = sjcl.encrypt(password, "Sprwadzanie glownego hasla", p, rp);
        masterPassword = password;
        chrome.runtime.sendMessage({ sender: "popupPassword", pass: masterPassword }, null);
        chrome.storage.local.set({ "testingMaster": ciphertext });
        console.log("[INFO] Update password");
    }
}

$(function () {
    preparePopup();
    setFunctionToButtons();
    prepareBackgroundData();
    console.log("[INFO]AutoWebLogin Loaded");
});