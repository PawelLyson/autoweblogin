﻿
console.log("[AutoWebLogin] ScriptLoaded");

var actualDate = new Date();
chrome.runtime.sendMessage({
    sender: "contentTabCreate", data: "contentJS", sendTime: actualDate.getTime()
}, null);

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.sender === "backgroundWorkerActivate") {
            loginToWeb(request, sender, sendResponse);
        }
        else if (request.sender === "backgroundCredentials") {
            loginWithCredentials(request, sender, sendResponse);
        }
    }
);

var loginElem = null;
var passwordElem = null;
var submitElem = null;

function loginToWeb(request, sender, sendResponse) {
    console.log(request.webInfo.passType);
    console.log(request);
    //if (request.webInfo.passType !== 0)


    //rp = {};
    //console.log(sjcl.decrypt(masterPassword, ciphertextCredentials, {}, rp));
    //var ciphertextCredentials = chrome.storage.local.get("credentials_" + startedTabs[tabID]["webInfo"].passType, saveCredentials);

    loginElem = $(request.webInfo["loginElem"])[0];//.value = request.login;
    passwordElem = $(request.webInfo["passwordElem"])[0];//.value = request.password;
    submitElem = $(request.webInfo["submitElem"])[0];
    chrome.runtime.sendMessage({ sender: "contentScriptCiphertext", passType: request.webInfo.passType });//writeWebCredentials);
}


function loginWithCredentials(request, sender, sendResponse) {
    console.log(sendResponse);
    loginElem.value = request.credentials.login;
    passwordElem.value = request.credentials.password;
    submitElem.click();
}