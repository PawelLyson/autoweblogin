﻿'use strict';

function preparePopup() {

    $('.selection-2').select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
    
    $('.input3').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() !== '') {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        });
    });
    
    $('#radio1, #radio2').on('change', function () {
        $('.input-newWeb').slideUp(300);
        $('.input-newLogin').slideUp(300);
        $('.input-changeWeb').slideUp(300);
        $('.input-changeLogin').slideUp(300);
    });

    $('#showAddCredentials').on('click', function () {
        cleanCredentialsForm();
        cleanWebForm();
        if ($('#radio1').is(':checked')) {
            $('.input-newLogin').slideDown(300);
        }
        else {
            $('.input-changeLogin').slideDown(300);
        }
        $('.input-newWeb').slideUp(300);
        $('.input-changeWeb').slideUp(300);
    });

    $('#showAddWeb').on('click', function () {
        cleanCredentialsForm();
        cleanWebForm();
        if ($('#radio1').is(':checked')) {
            $('.input-newWeb').slideDown(300);
        }
        else {
            loadStorageName();
            $('.input-changeWeb').slideDown(300);
        }
        $('.input-newLogin').slideUp(300);
        $('.input-changeLogin').slideUp(300);
    });

    $('#settingButton').on('click', function () {
        var settingsButtons = $('.input-settingsButtons');
        if (settingsButtons.css('display') === 'none') {
            $('.input-settingsButtons').slideDown(300);
            $('.input-websButtons').slideUp(300);
        }
        else {
            $('.input-settingsButtons').slideUp(300);
            $('.input-websButtons').slideDown(300);
            $('.input-newWeb').slideUp(300);
            $('.input-newLogin').slideUp(300);
        }
    });

    $('#loginNameSelect').on('select2:select', loadWebForm)
    $('#credentialsSelect').on('select2:select', loadCredentialsForm)
    
    $('.validate-form .input3').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

}
function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}

function loginToExtension() {
    $('.input-MainPassword').slideUp(300);
    $('.input-afterMainPassword').slideDown(300);
}

function setFunctionToButtons() {

    document.querySelector('#autoLogin').addEventListener(
        'click', openWebs);
    document.querySelector('#masterPasswordButton').addEventListener(
        'click', holdMasterPassword);
    document.querySelector('#addCredentials').addEventListener(
        'click', addEditCredentials);
    document.querySelector('#changeCredentials').addEventListener(
        'click', addEditCredentials);
    document.querySelector('#removeCredentials').addEventListener(
        'click', addEditCredentials);
    document.querySelector('#addWeb').addEventListener(
        'click', addEditWebs);
    document.querySelector('#changeWeb').addEventListener(
        'click', addEditWebs);
    document.querySelector('#removeWeb').addEventListener(
        'click', addEditWebs);
    
}
function prepareBackgroundData() {

    chrome.runtime.sendMessage({ 'sender': 'popupGetMasterPassword' }, setMasterPassword);
    chrome.storage.local.get('websToAutoLogin', loadWebsToAutoLogin);
    chrome.storage.local.get('credentials', loadCredentalsForSelect);

}