﻿function elementIsEmpty(object) {
    if (typeof object === "string") {
        return object.trim().length === 0;
    }
    for (var prop in object) if (object.hasOwnProperty(prop)) return false;
    return true;
}

function getValueFromJQueryString(jqueryString) {
    return $(jqueryString)[0].value;
}

function isEmptyJQueryString(jqueryString) {
    return elementIsEmpty(getValueFromJQueryString(jqueryString));
}

function cleanJQueryElementInput(jqueryString) {
    $(jqueryString)[0].value = "";
}